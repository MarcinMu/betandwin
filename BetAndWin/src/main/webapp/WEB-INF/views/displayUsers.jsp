<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">
<body>
	<h1>All Users</h1>

	<table class="table table-striped">
		<thead>
			<tr>
				<th>Name</th>
				<th>Surname</th>
			</tr>
		</thead>

		<c:forEach var="user" items="${users}">
			<tr>
				<td>${user.name}</td>
				<td>${user.surname}</td>
				<td>
					<spring:url value="/user/${user.surname}/delete" var="deleteUrl" />
					<form method="POST" action="${deleteUrl}">
					<input type="submit" value ="Delete" />
					</form>
				</td>
			</tr>
		</c:forEach>
	</table>
	
	<spring:url value="/user/add" var="addUserFormUrl" />
	<button class="btn btn-primary" onclick="location.href='${addUserFormUrl}'">Add user</button>

</body>
</html>