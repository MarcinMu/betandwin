package com.kedzier.spring.mvc.service;

import java.util.List;

public interface BetDao {
	
	void add(Bet bet);
	void update(Bet bet);
	List<Bet> getAll();
}
