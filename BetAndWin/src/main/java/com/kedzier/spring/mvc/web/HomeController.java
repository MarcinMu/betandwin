package com.kedzier.spring.mvc.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController {
	
	@RequestMapping("/")
	public ModelAndView helloWithRequestParam(@RequestParam(required = false) String name) {
		ModelAndView modelAndView = new ModelAndView("home");
		modelAndView.addObject("name", name != null ? name : "Stranger");
		return modelAndView;
	}
	
	@RequestMapping("/{name}")
	public ModelAndView helloWithPathVariable(@PathVariable String name) {
		ModelAndView modelAndView = new ModelAndView("home");
		modelAndView.addObject("name", name != null ? name : "Stranger");
		return modelAndView;
	}

}
