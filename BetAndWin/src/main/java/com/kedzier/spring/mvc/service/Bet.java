package com.kedzier.spring.mvc.service;

import java.sql.Time;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "bet")
public class Bet {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private int userId;
	
	private int matchId;
	
	private int hostScore;
	
	private int guestScore;
	
	private Time betTime;

	private int points;

	public final int getId() {
		return id;
	}

	public final void setId(int id) {
		this.id = id;
	}

	public final int getUserId() {
		return userId;
	}

	public final void setUserId(int userId) {
		this.userId = userId;
	}

	public final int getMatchId() {
		return matchId;
	}

	public final void setMatchId(int matchId) {
		this.matchId = matchId;
	}

	public final int getHostScore() {
		return hostScore;
	}

	public final void setHostScore(int hostScore) {
		this.hostScore = hostScore;
	}

	public final int getGuestScore() {
		return guestScore;
	}

	public final void setGuestScore(int guestScore) {
		this.guestScore = guestScore;
	}

	public final Time getBetTime() {
		return betTime;
	}

	public final void setBetTime(Time betTime) {
		this.betTime = betTime;
	}

	public final int getPoints() {
		return points;
	}

	public final void setPoints(int points) {
		this.points = points;
	}
	
	
	
}
